package com.android4dev.models;

import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android4dev.controllers.MainActivityController;
import com.android4dev.helpers.AnalyticsTrackers;
import com.android4dev.helpers.AppController;
import com.android4dev.helpers.LocationInfo;
import com.android4dev.helpers.ProductInfo;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Model in MVC Pattern
 * @author ZeNan Gao
 * @since 08/22/2015
 * @version 1.0
 */
public class MainActivityModel {
    private MainActivityController controller;
    private static String url="https://dl.dropboxusercontent.com/u/1559445/ASOS/SampleApi/anycat_products.json?catid=";
    private String getLocationPhp = "http://10.1.10.60/asos/get_all_products.php";
    private String catid;
    private ProgressDialog mDialog;

    public MainActivityModel(MainActivityController controller){
        this.controller = controller;
    }

    public void showProducts(String catid){
        this.catid = catid;
        makeJsonReq();
    }

    /**
     * perform JSON parsing to display products
     */
    private void makeJsonReq() {
        showProgressDialog();
        //Log.e("inside","inside makeJsonReq with "+url+catid);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url+catid,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Volley", response.toString());
                        ArrayList<ProductInfo> products = new ArrayList<>();
                        try {
                            JSONArray c = response.getJSONArray("Listings");
                            //Log.i("jarray",c.toString());
                            JSONObject info;
                            int pid;
                            int price;
                            String currentprice;
                            String imageurl;
                            ProductInfo pi;
                            for (int i = 0; i < c.length(); i++) {
                                info = c.getJSONObject(i);
                                currentprice = info.getString("CurrentPrice");
                                pid = info.getInt("ProductId");
                                imageurl = info.getJSONArray("ProductImageUrl").getString(0);
                                price = info.getInt("BasePrice");
                                //Log.i("info",currentprice +" "+ pid+" " + imageurl+" " +price);
                                pi = new ProductInfo(price, currentprice, imageurl, pid);
                                products.add(pi);
                            }
                        } catch (JSONException e) {
                            Log.d("Jsonerror", e.toString());
                        }
                        //Log.d("product",products.toString());

                        controller.view.showProducts(products);
                        //adapter.notifyDataSetChanged();

                        Tracker t = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
                        //t.enableAdvertisingIdCollection(true);
                        t.setScreenName("Recycler View");
                        // Build and Send the Analytics Event.
                        t.send(new HitBuilders.ScreenViewBuilder().build());
                        Log.i("sent", "sending data");
                        //setView();
                        //msgResponse.setText(response.toString());
                        hideProgressDialog();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("error", "Error: " + error.getMessage());
                hideProgressDialog();
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, "jSon_obj");
    }

    /**
     * show progress dialog while loading images
     */
    private void showProgressDialog(){
        mDialog = new ProgressDialog(controller.view);
        mDialog.setMessage("Loading Images, Please Wait");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    /**
     * hide progress dialog when loading finish
     */
    private void hideProgressDialog(){
        mDialog.dismiss();
    }

    public void startPaypal() {
        controller.view.startPaypal();
    }

    public void makeLocationRequest(){
        JsonObjectRequest req = new JsonObjectRequest(getLocationPhp,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Volley", response.toString());
                        try {
                            int success = response.getInt("success");
                            ArrayList<LocationInfo> locations = new ArrayList<>();
                            if (success == 1) {
                                String name, address;
                                double lat,lng;

                                // create parcelable array
                                JSONArray c = response.getJSONArray("markers");
                                for(int i=0; i < c.length();i++){
                                    JSONObject t = c.getJSONObject(i);
                                    name = t.getString("name");
                                    address = t.getString("address");
                                    lat = Double.parseDouble(t.getString("lat"));
                                    lng = Double.parseDouble(t.getString("lng"));
                                    locations.add(new LocationInfo(lat,lng,name,address));
                                }

                                controller.view.showLocation(locations);

                            } else {
                                Toast.makeText(controller.view, "Wrong PHP request", Toast.LENGTH_LONG).show();
                            }
                        }catch (JSONException e){
                            Log.d("JSONError",e.toString());
                        }catch (NumberFormatException e){
                            Log.e("Number Format Error",e.toString());
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyError", error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, "JSon_obj");
    }
}
