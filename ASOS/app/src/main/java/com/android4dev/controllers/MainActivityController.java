package com.android4dev.controllers;

import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android4dev.activities.MainActivity;
import com.android4dev.helpers.AnalyticsTrackers;
import com.android4dev.models.MainActivityModel;
import com.android4dev.navigationview.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Controller in MVC Pattern
 * @author ZeNan Gao
 * @since 8/22/2015
 * @version 1.0
 */
public class MainActivityController implements NavigationView.OnNavigationItemSelectedListener{
    public MainActivity view;
    private MainActivityModel model;

    public MainActivityController(MainActivity control){
        this.view = control;
        model = new MainActivityModel(this);
    }

    /**
     * register event listeners
     */
    public void startExecution(){
        view.navigationView.setNavigationItemSelectedListener(this);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(view,view.drawerLayout,view.toolbar,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank

                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        view.drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    /**
     * event handler based on menuItem.getItemId()
     * @param menuItem menu
     * @return True if the event is handled, false otherwise
     */
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        if (menuItem.isChecked()) menuItem.setChecked(false);
        else menuItem.setChecked(true);

        //Closing drawer on item click
        view.drawerLayout.closeDrawers();

        //Check to see which item was being clicked and perform appropriate action
        switch (menuItem.getItemId()) {
            case R.id.inbox:
                //Toast.makeText(getApplicationContext(),"Inbox Selected",Toast.LENGTH_SHORT).show(
                model.showProducts("catalog01_1000_2623");
                sendAnalyticData(R.id.inbox);
                return true;
            case R.id.show_map:
                model.makeLocationRequest();
                sendAnalyticData(R.id.show_map);
                return true;
            case R.id.starred:
                model.showProducts("catalog01_1000_6992");
                sendAnalyticData(R.id.starred);
                return true;
            case R.id.sent_mail:
                model.showProducts("catalog01_1000_6930");
                return true;
            case R.id.drafts:
                model.showProducts("catalog01_1000_12451");
                return true;
            case R.id.allmail:
                model.showProducts("catalog01_1000_4174");
                return true;
            case R.id.trash:
                model.showProducts("catalog01_1000_8730");
                return true;
            case R.id.spam:
                model.showProducts("catalog01_1000_1314");
                return true;
            case R.id.donate:
                model.startPaypal();
                return true;
            default:
                Toast.makeText(view, "Somethings Wrong", Toast.LENGTH_SHORT).show();
                return true;
        }
    }

    /**
     * Sending data
     * @param _id for identify sending source
     */
    private void sendAnalyticData(int _id){
        Tracker t = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        //t.enableAdvertisingIdCollection(true);
        t.setScreenName("Main Activity");
        // Build and Send the Analytics Event.
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
