package com.android4dev.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android4dev.controllers.MainActivityController;
import com.android4dev.fragments.Fragment_Map;
import com.android4dev.fragments.Fragment_Product_Detail;
import com.android4dev.helpers.AppController;
import com.android4dev.helpers.LocationInfo;
import com.android4dev.helpers.ProductInfo;
import com.android4dev.navigationview.R;
import com.android4dev.views.MyRecyclerView;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.plus.Plus;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;

/**
 * @author ZeNan Gao
 * @version 08/22/2015
 * @since 1.2
 */
public class MainActivity extends AppCompatActivity {

    //Defining Variables
    public Toolbar toolbar;
    public NavigationView navigationView;
    public DrawerLayout drawerLayout;
    private String url = "https://dl.dropboxusercontent.com/u/1559445/ASOS/SampleApi/anyproduct_details.json?catid=";
    private ArrayList<String> urlarray;
    private String description, brandname;
    private String personPhotoUrl,email,name,personGooglePlusProfile;
    // Profile pic image size in pixels
    private static final int PROFILE_PIC_SIZE = 400;
    /**
     * - Set to PaymentActivity.ENVIRONMENT_PRODUCTION to move real money.
     *
     * - Set to PaymentActivity.ENVIRONMENT_SANDBOX to use your test credentials
     * from https://developer.paypal.com
     *
     * - Set to PayPalConfiguration.ENVIRONMENT_NO_NETWORK to kick the tires
     * without communicating to PayPal's servers.
     */
    //set the environment for production/sandbox/no netowrk
    private static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    private static final String CONFIG_CLIENT_ID = "Af5Ju2loeWkGI8rzYTCeY4xYaest4IlRWpBozKLp2Y6-L-vVkm0PLJPJPKyiS_EoKt8yfepP9hlOA01m";
    private static final int REQUEST_PAYPAL_PAYMENT = 1;
    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(CONFIG_CLIENT_ID)
                    // The following are only used in PayPalFuturePaymentActivity.
            .merchantName("The Appexperts Ltd")
            .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));

    public MainActivityController controller;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get user info
        Bundle bd = getIntent().getBundleExtra("image");
        personPhotoUrl = bd.getString("url");
        email = bd.getString("email");
        name = bd.getString("name");
        personGooglePlusProfile = bd.getString("profile");

        //set user info view
        de.hdodenhof.circleimageview.CircleImageView view = (de.hdodenhof.circleimageview.CircleImageView) findViewById(R.id.profile_image);
        TextView  tv_username = (TextView)findViewById(R.id.username);
        TextView tv_email = (TextView) findViewById(R.id.email);
        tv_email.setText(email);
        tv_username.setText(name);
        // display circle photo for user
        personPhotoUrl = personPhotoUrl.substring(0,
                personPhotoUrl.length() - 2)
                + PROFILE_PIC_SIZE;
        new LoadProfileImage(view).execute(personPhotoUrl);
        //Log.i("info",personPhotoUrl + " "+ email+" "+ name +" "+ " "+ personGooglePlusProfile);

        //start google analytic report
        GoogleAnalytics.getInstance(this).reportActivityStart(this);

        // Initializing Toolbar and setting it as the actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer);

         // start pay pal services

        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(intent);

        // Start controller in MVC Pattern
        controller = new MainActivityController(this);
        controller.startExecution();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_PAYPAL_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        System.out.println("Responseeee" + confirm);
                        Log.i("paymentExample", confirm.toJSONObject().toString());


                        JSONObject jsonObj = new JSONObject(confirm.toJSONObject().toString());

                        String paymentId = jsonObj.getJSONObject("response").getString("id");
                        System.out.println("payment id:-==" + paymentId);
                        Toast.makeText(getApplicationContext(), paymentId, Toast.LENGTH_LONG).show();

                    } catch (JSONException e) {
                        Log.e("paymentExample", "an extremely unlikely failure occurred: ", e);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.i("paymentExample", "The user canceled.");
            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                Log.i("paymentExample", "An invalid Payment was submitted. Please see the docs.");
            }
        }
    }

    /**
     * Start paypal service
     */
    public void startPaypal(){
        PayPalPayment thingToBuy = new PayPalPayment(new BigDecimal(1), "USD", "EvilZ",
                PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(MainActivity.this, PaymentActivity.class);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);

        startActivityForResult(intent, REQUEST_PAYPAL_PAYMENT);
    }
    /**
     * show fragment with product array obtain from model in MVC Pattern
      */
    public void showProducts(ArrayList<ProductInfo> products){
        Fragment fragment = new MyRecyclerView();
        Bundle bd = new Bundle();
        bd.putParcelableArrayList("products",products);
        fragment.setArguments(bd);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

    /**
     * get data from localhost database using php
     * parse data and start map fragment
     */
    public void makeLocationRequest(){
        //Log.i("location", getLocationPhp);

    }

    public void showLocation(ArrayList<LocationInfo> locations){
        // pass locations to start showing map
        Fragment fragment = new Fragment_Map();
        Bundle bd = new Bundle();
        bd.putParcelableArrayList("locations",locations);
        fragment.setArguments(bd);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }
    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     * @param menu menu ref to be inflated
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Handle action bar item clicks here. The action bar will
     * automatically handle clicks on the Home/Up button, so long
     * as you specify a parent activity in AndroidManifest.xml.
     * @param item item that is selected
     * @return true if handled
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_signout:
                signOutFromGplus();
                returnToLogin();
                return true;
            case R.id.action_revokeaccess:
                revokeGplusAccess();
                returnToLogin();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * return to Login Page after sign out
     */
    private void returnToLogin(){
        Intent intent = new Intent(this, Login.class);
        startActivity(intent);
    }

    /**
     * invoked from recycler view of list of products
     * @param pid product id
     */
    public void showProduct(int pid){
        makeJsonReq(pid);
    }

    /**
     * perform Json parsing and start new fragment
     * @param pid product id
     */
    private void makeJsonReq(int pid) {
        final int pID = pid;
        //Log.e("inside","inside makeJsonReq with "+url+catid);
        JsonObjectRequest req = new JsonObjectRequest(Request.Method.GET,url+pid,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Log.d("Volley", response.toString());
                        urlarray = new ArrayList<>();
                        try {
                            // add to urlArray
                            brandname = response.getString("Brand");
                            description = response.getString("Description");
                            JSONArray c = response.getJSONArray("ProductImageUrls");
                            for(int i=0; i < c.length();i++){
                                urlarray.add(c.getString(i));
                            }
                        }catch (JSONException e){
                            Log.d("JSONError",e.toString());
                        }

                        // start show product fragment
                        Bundle bd = new Bundle();
                        Fragment fr = new Fragment_Product_Detail();
                        bd.putString("desc",description);
                        bd.putString("brand", brandname);
                        bd.putStringArrayList("list", urlarray);
                        bd.putInt("pid", pID);
                        //Log.i("bundle",bd.toString());
                        fr.setArguments(bd);
                        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.frame,fr);
                        ft.commit();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("VolleyError", error.getMessage());
            }
        });

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req, "jSon_obj");

    }

    /**
     * when activity, also stop user connection
     */
    @Override
    protected void onStop() {
        super.onStop();
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        Login.mGoogleApiClient.disconnect();
    }

    /**
     * stop connection
     */
    private void signOutFromGplus() {
        if (Login.mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(Login.mGoogleApiClient);
            Login.mGoogleApiClient.disconnect();
            Login.mGoogleApiClient.connect();
        }
    }

    /**
     * Revoking access from google
     */
    private void revokeGplusAccess() {
        if (Login.mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(Login.mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(Login.mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Log.e("revoke", "User access revoked!");
                            Login.mGoogleApiClient.connect();
                            //updateUI(false);
                        }

                    });
        }
    }

    /**
     * inner class to process circle image using AsyncTask with url
     */
    private class LoadProfileImage extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public LoadProfileImage(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
