package com.android4dev.helpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * parcelable class for location data
 * @author ZeNan Gao
 * @version 08/15/2015
 * @since 1.0
 */
public class LocationInfo implements Parcelable {
    //define variables
    public double latitude;
    public double longitude;
    public String name;
    public String address;

    /**
     * constructor to create object from parcel
     */

    public LocationInfo(Parcel source) {
        latitude  = source.readDouble();
        longitude = source.readDouble();
        name = source.readString();
        address = source.readString();
    }

    /**
     * constructor
     * @param latitude location latitude
     * @param longitude location longitude
     * @param name location name
     * @param address location address
     */
    public LocationInfo(double latitude,double longitude, String name, String address) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.address = address;
    }

    public int describeContents() {
        return this.hashCode();
    }

    /**
     * write to parcel obj
     * @param dest parcel to be written into
     * @param flags not used
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(name);
        dest.writeString(address);
    }

    /**
     * re-create obj from parcel
     */
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public LocationInfo createFromParcel(Parcel in) {
            return new LocationInfo(in);
        }

        public LocationInfo[] newArray(int size) {
            return new LocationInfo[size];
        }
    };

}