package com.android4dev.helpers;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * @author ZeNan Gao
 * @version 08/07/2015
 * @since 1.0
 */
public class ProductInfo implements Parcelable{
    private int mPrice;
    private String pUrl;
    private String currentPrice;
    private int pid;

    /**
     * Constructor to create object from parcel
     * @param source source parcel
     */
    public ProductInfo(Parcel source){
        mPrice = source.readInt();
        pUrl = source.readString();
        currentPrice = source.readString();
        pid = source.readInt();
    }
    /**
     * Constructor
     * @param mPrice price
     * @param currentPrice current price
     * @param pUrl product image url
     * @param pid product id
     */
    public ProductInfo(int mPrice,String currentPrice,String pUrl,int pid){
        this.mPrice = mPrice;
        this.currentPrice = currentPrice;
        this.pUrl = pUrl;
        this.pid = pid;
    }

    /**
     * get price
     * @return price
     */
    public int getmPrice(){return mPrice;}

    /**
     * get current price
     * @return current price
     */
    public String getCurrentPrice(){return currentPrice;}

    /**
     * get product image url
     * @return product image url
     */
    public String getpUrl(){return pUrl;}

    /**
     * get product id
     * @return product id
     */
    public int getPid(){return pid;}

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    /**
     * write to parcel obj
     * @param dest dest parcle
     * @param flags not used
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mPrice);
        dest.writeString(currentPrice);
        dest.writeString(pUrl);
        dest.writeInt(pid);
    }

    /**
     * re-create obj from parcel
     */
    public static final Parcelable.Creator CREATOR
            = new Parcelable.Creator() {
        public ProductInfo createFromParcel(Parcel in) {
            return new ProductInfo(in);
        }

        public ProductInfo[] newArray(int size) {
            return new ProductInfo[size];
        }
    };
}
