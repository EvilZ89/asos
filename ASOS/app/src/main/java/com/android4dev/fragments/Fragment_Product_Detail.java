package com.android4dev.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android4dev.helpers.AnalyticsTrackers;
import com.android4dev.navigationview.R;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.ArrayList;

/**
 * class to populate details and implement viewgroup
 * @author ZeNan Gao
 * @since 1.0
 * @version 8/7/2015
 */
public class Fragment_Product_Detail extends Fragment {
    //define variables
    private int pid;
    private String brand,description;
    private TextView tvBrand;
    private TextView tvDec;
    private ViewPager vpPic;
    private PagerAdapter mAdapter;
    static ArrayList<String> urls;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_product_detail, container, false);

        //initialize view components
        tvBrand = (TextView)rtView.findViewById(R.id.fragment_product_detail_tv_brand);
        tvDec = (TextView)rtView.findViewById(R.id.fragment_product_detail_tv_des2);
        vpPic = (ViewPager)rtView.findViewById(R.id.fragment_product_detail_vp_pic);

        // get urls, brand, desc
        urls = new ArrayList<>();
        Bundle bd = getArguments();
        brand = bd.getString("brand");
        description = bd.getString("desc");
        urls = bd.getStringArrayList("list");
        pid = bd.getInt("pid");

        sendAnalyticData(pid);
        //Log.i("des",description);
        tvBrand.setText(brand);
        tvDec.setText(description);
        mAdapter = new MyAdapter(getChildFragmentManager());
        vpPic.setAdapter(mAdapter);
        return rtView;
    }

    /**
     * sending analytic data
     * @param _id product id
     */
    private void sendAnalyticData(int _id){
        Tracker t = AnalyticsTrackers.getInstance().get(AnalyticsTrackers.Target.APP);
        //t.enableAdvertisingIdCollection(true);
        t.setScreenName("Show Products");
        // Build and Send the Analytics Event.
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    /**
     * static inner class for process page viewer
     */
    public static class MyAdapter extends FragmentPagerAdapter {
        public MyAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public int getCount() {
            return urls.size();
        }

        @Override
        public Fragment getItem(int position) {
            //create image fragment for each page
            Fragment fragment = new Fragment_pic();
            Bundle bd = new Bundle();
            bd.putString("url", urls.get(position));
            fragment.setArguments(bd);
            return fragment;
        }
    }

}
