package com.android4dev.fragments;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.android4dev.helpers.LocationInfo;
import com.android4dev.navigationview.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

/**
 * Fragment class for display markers on map
 * @author ZeNan Gao
 * @since 08/15/2015
 * @version 1.1
 */
public class Fragment_Map extends Fragment implements
        OnMapReadyCallback {

    private GoogleMap mMap;
    private ProgressDialog mDialog;

    private LatLngBounds bounds;
    /**
     * inflate the view and sync google map
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_map,container,false);
        SupportMapFragment mapFragment =
                ((SupportMapFragment) getChildFragmentManager()
                        .findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
        return rtView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Hide the zoom controls as the button panel will cover it.
        mMap.getUiSettings().setZoomControlsEnabled(false);

        // Add lots of markers to the map.
        addMarkersToMap();
    }

    /**
     * retrieve data from parcelable array and populate the markers
     * set view on map
     */
    private void addMarkersToMap(){

        showProgressDialog();
        Bundle bd = getArguments();
        final ArrayList<LocationInfo> locations = bd.getParcelableArrayList("locations");
        //Log.i("locations",locations.toString());
        // add markers
        for(LocationInfo x : locations){
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(x.latitude,x.longitude))
                    .title(x.name)
                    .snippet(x.address));
        }

        //zoom in to show markers
        final View mapView = getChildFragmentManager().findFragmentById(R.id.map).getView();
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @SuppressWarnings("deprecation") // We use the new method when supported
                @SuppressLint("NewApi") // We check which build version we are using.
                @Override
                public void onGlobalLayout() {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for(LocationInfo x: locations){
                        builder.include(new LatLng(x.latitude,x.longitude));
                    }
                    bounds = builder.build();
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                        mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                        mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                }
            });
        }

        hideProgressDialog();

    }
    /**
     * show progress dialog while loading map
     */
    private void showProgressDialog(){
        mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Loading Markers on Maps, Please Wait");
        mDialog.setCancelable(false);
        mDialog.show();
    }

    /**
     * hide progress dialog when loading finish
     */
    private void hideProgressDialog(){
        mDialog.dismiss();
    }
}