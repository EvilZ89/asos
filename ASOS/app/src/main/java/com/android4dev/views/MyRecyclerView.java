package com.android4dev.views;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android4dev.adapters.MyAdapter;
import com.android4dev.helpers.ProductInfo;
import com.android4dev.navigationview.R;

import java.util.ArrayList;

/**
 * @author ZeNan Gao
 * @version 08/07/2015
 * @since 1.0
 */
public class MyRecyclerView extends Fragment{
    //define variables
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<ProductInfo> products;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rtView = inflater.inflate(R.layout.fragment_products,container,false);
        //STEP 1: Initialize the RecyclerView
        recyclerView = (RecyclerView) rtView.findViewById(R.id.fragment_products_rv_recyclerView);
        recyclerView.setHasFixedSize(true);

        //STEP 2: Set the Layout
        //layoutManager = new GridLayoutManager(this,2);
        layoutManager = new GridLayoutManager(getActivity(),2,GridLayoutManager.VERTICAL,false);
       // layoutManager = new LinearLayoutManager(getActivity());
        //  layoutManager = new StaggeredGridLayoutManager(2,1);
        recyclerView.setLayoutManager(layoutManager);

        //STEP 3: Set the Animator
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        products = new ArrayList<ProductInfo>();

        Bundle bd = getArguments();
        products = bd.getParcelableArrayList("products");
        //STEP 4: Set Adapters
        adapter = new MyAdapter(getActivity(),products);
        recyclerView.setAdapter(adapter);

        return rtView;
    }


}
